module.exports = (env, argv) => {
    console.log(env);
    /** Constants */
    if (!env) {
        env = {};
        env['variables'] = 'ABC';
    }
    const HtmlWebpackPlugin = require('html-webpack-plugin'),
        CopyWebpackPlugin = require('copy-webpack-plugin'),
        loaders = require('./webpack/webpack_loaders'),
        HtmlBeautifyPlugin = require('html-beautify-webpack-plugin'),
        path = require('path'),
        srcPath = './src',
        pugPath = srcPath + '/pug/',
        assetsPath = 'assets',
        imagePath = assetsPath + '/images',
        jsPath = srcPath + '/' + assetsPath + '/js/',
        modalPath = pugPath + 'templates/modals/',
        licenseDetailPath = pugPath + 'license-detail/',
        pugFileNames = ['temporary-license', 'index', 'signup', 'dashboard', 'application', 'modal', 'profile', 'renew-license','forgot-password','reset-password'];
    var plugins = [];

    /** Addind the pug files */
    pugFileNames.forEach(element => {
        plugins.push(
            new HtmlWebpackPlugin({
                template: pugPath + element + '.pug',
                filename: element + '.html',
                variables: env.variables
            })
        );
    });

    /** Adding license files */
    var fs = require('fs');
    var files = fs.readdirSync(licenseDetailPath);
    files.forEach(element => {
        plugins.push(
            new HtmlWebpackPlugin({
                template: licenseDetailPath + element,
                filename: 'license-detail/' + element.split('.pug')[0] + '.html',
                variables: env.variables
            })
        );
    });


    /** Adding modal files */
    var files = fs.readdirSync(modalPath);
    files.forEach(element => {
        plugins.push(
            new HtmlWebpackPlugin({
                template: modalPath + element,
                filename: 'modals/' + element.split('.pug')[0] + '.html',
                variables: env.variables
            })
        );
    });

    plugins.push(new HtmlBeautifyPlugin({
        config: {
            html: {
                end_with_newline: true,
                indent_size: 2,
                indent_with_tabs: true,
                indent_inner_html: true,
                preserve_newlines: true,
                unformatted: ['p', 'i', 'b', 'span']
            }
        },
        replace: [' type="text/javascript"']
    }));

    /** Copy plugin for copy the iamges */
    plugins.push(new CopyWebpackPlugin([
        { from: srcPath + '/' + imagePath, to: imagePath }
    ]));


    /** config object for the webpack */

    const config = {
        //watch: true, // uncomment this line for live rebuild
        plugins: plugins,
        module: {
            /** defining the rules for the files */
            rules: [
                loaders.fontLoader,
                loaders.imageLoader,
                loaders.jsLoader,
                loaders.pugLoader,
                loaders.sassLoader(env.variables)
            ]
        },
        entry: {
            entry: srcPath + '/entry.js',
            app: jsPath + 'app.js'
        },
        output: {
            path: path.resolve(__dirname, 'dist/' + env.variables),
            filename: (path, context) => {
                console.log(path.chunk.entryModule.context);
                if (path.chunk.entryModule.context.indexOf('assets') != -1) {
                    return "assets/js/[name].js";
                } else {
                    return "[name].bundle.js";
                }
            },
        },
        devtool: 'source-map',
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 2222,
            open: true,
        }
    };
    if (argv.mode === 'development') {}
    if (argv.mode === 'production') {}
    return config;
}