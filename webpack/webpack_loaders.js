const
    assetsPath = 'assets',
    fontPath = assetsPath + '/fonts',
    cssPath = assetsPath + '/css',
    path = require('path');

module.exports = {
    /** Load PUG files */
    pugLoader: {
        test: /\.pug$/,
        use: ['pug-loader']
    },
    /** Load JS files */
    jsLoader: {
        test: /\.js$/,
        use: {
            loader: "babel-loader",
            options: { presets: ["@babel/preset-env"] }
        }
    },
    /** Load sass files */
    sassLoader: (mode) => {
        console.log("hello:" + mode);

        return {
            test: /\.sass$/,
            use: [{
                    loader: "file-loader",
                    options: {
                        name: (path) => {
                            if (path.indexOf('pages') != -1) {
                                return cssPath + "/pages/[name].css";
                            } else {
                                return cssPath + "/[name].css";
                            }
                        },
                        // sourceMap: true
                    },
                },
                {
                    loader: "extract-loader",
                    options: {
                        // dynamically return a relative publicPath based on how deep in directory structure the loaded file is in /src/ directory
                        publicPath: (context) => '../'.repeat(path.relative(path.resolve('src'), context.context).split('/').length),
                        // sourceMap: true
                    }
                },
                {
                    loader: "css-loader",
                    options: {
                        // sourceMap: true
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: () => [require('autoprefixer')({
                            'Browserslist': ['> 1%', 'last 2 versions']
                        })],
                        // sourceMap: true
                    },
                },
                {
                    loader: "sass-loader",
                    options: {
                        // sourceMap: true,
                        data: "$variables: " + mode + ";"
                    }
                }
            ]
        }
    },
    /** Load image files */
    imageLoader: {
        test: /\.(png|jpg)$/,
        loader: 'file-loader?name=/img/[name].[ext]'
    },
    /** FONT loader */
    fontLoader: {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: fontPath
            },

        }]
    }
}